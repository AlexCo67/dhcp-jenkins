# Projet DHCP  
*  COLOMBO Alexandre  
*  HOFFMAN Martin  
Système de mise à jour automatique d'une config DHCP via containers Docker et intégration continue
# Containers
## git_repo : Container du dépôt Git de la config DHCP

### Lancement de l'image
```bash
docker-compose up

```
### Récupération du repository Git
```bash
git clone git@localhost:dhcp-config.git
```

### Test du repository Git  
```bash
touch test.txt && echo "Bonjour" > test.txt
git add .
git commit -m "Test commit"
git push 
```
