FROM jenkins/jenkins:lts-jdk11
LABEL description="Jenkins gérant le déploiement de la config DHCP"
LABEL version="1.0"

USER root

# Updating container
RUN apt update && apt-get upgrade -y
RUN apt install -y isc-dhcp-server
RUN apt clean && apt-get autoremove

# Installing Docker
RUN curl -fsSL https://get.docker.com -o get-docker.sh 
RUN bash get-docker.sh
RUN curl -L \
  "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" \
  -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose